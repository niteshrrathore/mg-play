import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool mButtonState = true;
  late bool mAudioPlaying = false;
  late AudioPlayer player;
  late String mp3Uri = "new_ringtone.mp3";

  final AudioCache _audioCache = AudioCache(
    prefix: 'assets/audio/',
    fixedPlayer: AudioPlayer()..setReleaseMode(ReleaseMode.STOP),
  );

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
    super.initState();
  }

  @override
  void dispose() {
    _stopFile();
    super.dispose();
  }

  void _playFile() async {
    player = await _audioCache.play(mp3Uri); // assign player here
    mAudioPlaying = true;
    player.onPlayerCompletion.listen((event) {
      mButtonState = true;
      setState(() {});
    });
  }

  void _stopFile() {
    mAudioPlaying = false;
    player.stop(); // stop the file like this
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        mButtonState = true;
                        mp3Uri = "iphone_ringtone.mp3";
                        if(mAudioPlaying) {
                          _stopFile();
                        }

                        setState(() {});
                      },
                      child: Container(
                        padding: EdgeInsets.all(25.0),
                        decoration: BoxDecoration(color: Colors.white),
                        child: Text(
                          "First",
                          style: TextStyle(color: Colors.white, fontSize: 25),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        mButtonState = true;
                        mp3Uri = "boss_ringtone.mp3";
                        if(mAudioPlaying) {
                          _stopFile();
                        }

                        setState(() {});
                      },
                      child: Container(
                          padding: EdgeInsets.all(25.0),
                          decoration: BoxDecoration(color: Colors.white),
                          child: Text(
                            "Second",
                            style: TextStyle(color: Colors.white, fontSize: 25),
                          )),
                    )
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (mButtonState) {
                      mButtonState = false;
                      _playFile();
                    } else {
                      mButtonState = true;
                      _stopFile();
                    }
                  });
                },
                child: Container(
                  height: 150.0,
                  width: 150.0,
                  decoration: BoxDecoration(color: Colors.white),
                  child: Column(
                    children: <Widget>[
                      Expanded(
                          child: FittedBox(
                              child: (mButtonState)
                                  ? Icon(Icons.play_arrow_rounded)
                                  : Icon(Icons.pause_circle_outline_rounded))),
                    ],
                  ),
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: (){
                        mButtonState = true;
                        mp3Uri = "lovetone.mp3";
                        if(mAudioPlaying) {
                          _stopFile();
                        }
                        setState(() {});
                      },
                      child: Container(
                        padding: EdgeInsets.all(25.0),
                        decoration: BoxDecoration(color: Colors.white),
                        child: Text(
                          "Third",
                          style: TextStyle(color: Colors.white, fontSize: 25),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        mButtonState = true;
                        mp3Uri = "pushpa_oo_bolega.mp3";
                        if(mAudioPlaying) {
                          _stopFile();
                        }
                        setState(() {});
                      },
                      child: Container(
                        padding: EdgeInsets.all(25.0),
                        decoration: BoxDecoration(color: Colors.white),
                        child: Text(
                          "Fourth",
                          style: TextStyle(color: Colors.white, fontSize: 25),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ]),
      ),
    );
  }
}
